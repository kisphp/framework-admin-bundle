
### Add menu items

Register `ProjectMenuManager` service that will extend `Kisphp\FrameworkAdminBundle\Services\MenuManager`
```yaml
menu_manager:
    class: AppBundle\Services\ProjectMenuManager
    public: true
```


### Add media manager factory

```php
<?php

namespace AppBundle\Services;

use Kisphp\FrameworkAdminBundle\Services\AbstractMediaManagerFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MediaManagerFactory extends AbstractMediaManagerFactory
{
    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    protected function registerMediaModels(ContainerInterface $container)
    {
        $this->addMediaModels($container->get('model.article'));
        $this->addMediaModels($container->get('model.category'));
        $this->addMediaModels($container->get('model.cms.layout.widget'));
        $this->addMediaModels($container->get('model.shop.product'));
        // add here any other model classes you want to allow upload files to
    }
}
```

### Add AdminUser Entity

```php
<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Kisphp\FrameworkAdminBundle\Entity\AdminUser as KisphpAdminUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="admin_users", options={"collate": "utf8_general_ci", "charset": "utf8"})
 */
class AdminUser extends KisphpAdminUser
{
    /**
     * @var string
     * 
     * @ORM\Column(type="string", length=128)
     */
    protected $jenkins_token;
    
    /**
     * @return string
     */
    public function getJenkinsToken()
    {
        return $this->jenkins_token;
    }

    /**
     * @param string $jenkins_token
     */
    public function setJenkinsToken($jenkins_token)
    {
        $this->jenkins_token = $jenkins_token;
    }
}

```

### Copy assets dependencies

`package.json`

```javascript
{
    "dependencies": {
        "lightbox2": "^2.11.3"
    }
}
```

### Copy assets configiration

```javascript
// gulp-config.js
const settings = function(){
    this.root_path = __dirname;
    this.public_dir = "web";
    this.project_assets = __dirname + "/app/gulp/";
    this.short_commit_hash = process.env.COMMIT_SHORT_SHA ? process.env.COMMIT_SHORT_SHA : "latest";

    this.settings = {
        "name": "kisphp demo",
        "root_path": this.root_path,
        "project_assets": this.project_assets,

        "js": {
            "external": {
                "sources": [
                    'node_modules/jquery/dist/jquery.min.js',
                    'web/bundles/frameworkadmin/plugins/jquery-ui/jquery-ui.min.js',
                    'node_modules/jquery-ui/ui/widgets/datepicker.js',
                    'node_modules/jquery-ui/ui/widgets/sortable.js',
                    'node_modules/bootstrap/dist/js/bootstrap.min.js',
                    'node_modules/sweetalert/dist/sweetalert.min.js',
                    'node_modules/kisphp-format-string/src/format-string.js',
                    'node_modules/codemirror/lib/codemirror.js',
                    'node_modules/lightbox2/dist/js/lightbox.js',
                    this.project_assets + '/assets/dropzone/dropzone.js',
                    this.project_assets + '/assets/admin_lte/js/adminlte.js'
                ],
                "output_filename": "external-" + this.short_commit_hash + ".js",
                "output_dirname": this.public_dir + "/js/",

            },
            "project": {
                "sources": [
                    this.project_assets + '/assets/js/app.js',
                ],
                "output_filename": "app-" + this.short_commit_hash + ".js",
                "output_dirname": this.public_dir + "/js/",
            }
        },
        "css": {
            "external": {
                "sources": [
                    'web/bundles/frameworkadmin/plugins/jquery-ui/jquery-ui.min.css',
                    'node_modules/jquery-ui/themes/base/datepicker.css',
                    'node_modules/jquery-ui/themes/base/sortable.css',
                    'node_modules/bootstrap/dist/css/bootstrap.min.css',
                    'node_modules/bootstrap/dist/css/bootstrap-theme.min.css',
                    'node_modules/font-awesome/css/font-awesome.min.css',
                    'node_modules/codemirror/lib/codemirror.css',
                    'node_modules/lightbox2/dist/css/lightbox.css',
                    this.project_assets + '/assets/admin_lte/css/admin-lte.css',
                    this.project_assets + '/assets/admin_lte/css/skins/skin-blue.css'
                ],
                "output_filename": "external-" + this.short_commit_hash + ".css",
                "output_dirname": this.public_dir + "/css/",
            },
            "project": {
                "sources": [
                    this.project_assets + '/assets/dropzone/dist/basic.css',
                    this.project_assets + '/assets/dropzone/dist/dropzone.css',
                    this.project_assets + '/assets/css/main.styl',
                ],
                "output_filename": "app-" + this.short_commit_hash + ".css",
                "output_dirname": this.public_dir + "/css/",
            }
        },
        "files": {
            "fonts": {
                "sources": [
                    'node_modules/bootstrap/fonts/*.*',
                    'node_modules/font-awesome/fonts/*.*',
                ],
                "output_dirname": this.public_dir + "/webfonts"
            },
            "webfonts": {
                "sources": [
                    'node_modules/@fortawesome/fontawesome-free/webfonts/*.*'
                ],
                "output_dirname": "web/webfonts"
            },
            "img": {
                "sources": [
                    this.project_assets + 'assets/admin_lte/img/**/*.*'
                ],
                "output_dirname": this.public_dir + "/img"
            },
            "images": {
                "sources": [
                    "node_modules/lightbox2/dist/images/**/*.*"
                ],
                "output_dirname": this.public_dir + "/images"
            },
            "file-icons": {
                "sources": [
                    'vendor/kisphp/media-files-bundle/assets/file-icons/*.*'
                ],
                "output_dirname": this.public_dir + "/file-icons"
            },
            "favicons": {
                "sources": [
                    "vendor/kisphp/framework-admin-bundle/assets/favicon/*.*"
                ],
                "output_dirname": this.public_dir + "/favicon"
            }
        }
    };

    return this.settings;
};

module.exports = settings();
```
