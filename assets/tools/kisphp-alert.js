let KisphpAlert = new function() {
  let self = this;

  self.imageRemoved = function(){

  };

  self.init = function(){
    self.clean();
  };
  self.success = function(message){
    swal("Success", message, "success");
  };
  self.error = function(message) {
    swal("Error", message, "error");
  };
  self.info = function(message){
    return self.custom(message);
  };
  self.custom = function(message, title){
    let data = {
      "message": message,
      "title": title || "Info"
    };
    self.clangeClass('alert-info');
    self.displayAlert(data);
  };
  self.clean = function() {
    $('#modal-content').removeClass([
      'alert-success',
      'alert-danger',
      'alert-info',
      'alert-warning'
    ]);
    $('#modal-title').html('');
    $('#modal-body').html('');
  };
  self.clangeClass = function(className){
    self.clean();
    $('#modal-content').addClass(className);
  };
  self.displayAlert = function(options){
    $('#modal-title').html(options.title);
    $('#modal-body').html(options.message);
    self.show();
  };
  self.show = function(){
    $('#modal-alert').modal('show');
  };
  self.init();
};

module.exports = KisphpAlert;
