let alr = require('./kisphp-alert');
let call = require('./kisphp-callback');

let KisphpAjax = new function() {
  let self = this;

  /** if ajax url is null, the action will be in the same page */
  self.url = null;

  self.dataType = 'json';

  /**
   * @param newUrl
   * @returns {KisphpAjax}
   */
  self.setUrl = function(newUrl){
    self.url = newUrl;

    return self;
  };

  /**
   * @param newDataType
   * @returns {KisphpAjax}
   */
  self.setDataType = function(newDataType){
    self.dataType = newDataType;

    return self;
  };

  /**
   * makes Ajax call and then call a callback function with the response as parameter
   *
   * @param options
   * @param callbackFunction
   */
  self.ajaxSubmit = function(options, callbackFunction) {
    $.ajax({
      url: this.url,
      type: 'post',
      dataType: this.dataType,
      data: options
    })
      .done(function(response, textStatus, xhr){
        if (typeof callbackFunction === 'function') {
          return callbackFunction(response, xhr);
        } else if (typeof callbackFunction === 'string') {
          return call[callbackFunction](response, xhr);
        } else {
          return {ajaxResponse: response, xhr: xhr};
        }
      })
      .fail(function(response){
        if (parseInt(response.status) === 403) {
          alr.error('Permission denied');

          return null;
        }
      });
  };
};

module.exports = KisphpAjax;
