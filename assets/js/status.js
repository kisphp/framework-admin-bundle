let KisphpAjax = require('../tools/kisphp-ajax');
let KisphpAjaxCallbacks = require('../tools/kisphp-callback');
let KisphpAlert = require('../tools/kisphp-alert');

KisphpAjax.statusObjectToggle = function(options, url){
  this.setUrl(url).ajaxSubmit(options, 'toggleStatus');
};

KisphpAjaxCallbacks.toggleStatus = function(ajaxResponse) {

  let cssClass = {
    active: 'fa-toggle-on text-success',
    inactive: 'fa-toggle-off text-default'
  };

  if (ajaxResponse.code === 200) {
    let element = $('#status-' + ajaxResponse.objectId);
    if (ajaxResponse.status === 2) {
      element.find('.fa')
        .removeClass(cssClass.inactive)
        .addClass(cssClass.active);
    } else {
      element.find('.fa')
        .addClass(cssClass.inactive)
        .removeClass(cssClass.active);
    }
    return null;
  }

  KisphpAlert.error(ajaxResponse.message);
};

module.exports = {
  init: function() {
    $('.status-toggle').click(function(e){
      e.preventDefault();
      let id = $(this).data('id');
      let url = $(this).data('url');

      KisphpAjax.statusObjectToggle({
        id: id
      }, url);
    });
  }
};
