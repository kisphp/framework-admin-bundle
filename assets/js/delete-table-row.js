'use strict';

let KisphpAjax = require('../tools/kisphp-ajax');
let KisphpAjaxCallbacks = require('../tools/kisphp-callback');
let KisphpAlert = require('../tools/kisphp-alert');

let deleteCallback = function(ajaxResponse){
  if (parseInt(ajaxResponse.code) === 200) {
    $('#row-' + ajaxResponse.objectId).addClass('danger').fadeOut('slow');

    return null;
  }

  KisphpAlert.error(ajaxResponse.message);
};

KisphpAjax.deleteRow = function(options, url){
  this.setUrl(url).ajaxSubmit(options, deleteCallback);
};

let Delete = new function() {
  let self = this;

  self.init = () => {
    $('.delete-item').click(function(e){
      e.preventDefault();

      const id = $(this).data('id');
      const url = $(this).data('url');

      Swal.fire({
        title: trans.swal.remove.confirm.title,
        text: trans.swal.remove.confirm.text,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
      })
        .then((result) => {
          if (result.isConfirmed) {
            KisphpAjax.deleteRow({
              id: id
            }, url);
          }
        });
    });
  };
};

module.exports = Delete;
