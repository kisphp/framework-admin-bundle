<?php

namespace Kisphp\FrameworkAdminBundle\Fixtures;

use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractDemoData implements DemoDataInterface
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    /**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected function getContainer()
    {
        return $this->container;
    }
}
