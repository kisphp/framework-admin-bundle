<?php

namespace Kisphp\FrameworkAdminBundle\Fixtures;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

interface DemoDataInterface
{
    public function loadDemoData(InputInterface $input, OutputInterface $output);
}
