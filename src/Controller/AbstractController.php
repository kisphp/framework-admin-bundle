<?php

namespace Kisphp\FrameworkAdminBundle\Controller;

use Kisphp\Entity\FileInterface;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Entity\ToggleableInterface;
use Kisphp\Utils\Status;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Exception\InvalidArgumentException;

abstract class AbstractController extends Controller
{
    const EDIT_PATH = 'edit_path';
    const LIST_PATH = 'list_path';
    const ADD_PATH = 'add_path';
    const DELETE_PATH = 'delete_path';
    const STATUS_PATH = 'status_path';

    const SECTION_TITLE = '';
    const MODEL_NAME = '';
    const ENTITY_FORM_CLASS = '';
    const LISTING_TEMPLATE = '@FrameworkAdmin/components/listing.html.twig';
    const EDIT_TEMPLATE = '@FrameworkAdmin/components/form.html.twig';

    const HTML_EDITOR_ENABLED = false;
    const UPLOAD_TYPE = '';

    const ALLOW_SEARCH = false;

    /**
     * @var array
     */
    protected $section = [];

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function deleteAction(Request $request)
    {
        $id = $request->request->getInt('id');

        /** @var \Kisphp\FrameworkAdminBundle\Model\AbstractModel $model */
        $model = $this->get(static::MODEL_NAME);

        /** @var KisphpEntityInterface|ToggleableInterface $entity */
        $entity = $model->find($id);

        if ($entity === null) {
            return new JsonResponse([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => $this->trans('alert.message.delete.entity_not_found'),
            ]);
        }

        $model->remove($entity);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'objectId' => $id,
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function statusAction(Request $request)
    {
        $id = $request->request->getInt('id');

        /** @var \Kisphp\FrameworkAdminBundle\Model\AbstractModel $model */
        $model = $this->get(static::MODEL_NAME);

        /** @var KisphpEntityInterface|ToggleableInterface $entity */
        $entity = $model->find($id);

        if ($entity === null) {
            return new JsonResponse([
                'code' => Response::HTTP_NOT_FOUND,
                'message' => $this->trans('alert.message.status.entity_not_found'),
            ]);
        }

        $this->toggleStatus($entity);
        $model->save($entity);

        return new JsonResponse([
            'code' => Response::HTTP_OK,
            'objectId' => $entity->getId(),
            'status' => $entity->getStatus(),
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, $id = 0)
    {
        $form = $this->createEditForm($request, $id);
        $entity = $this->get(static::MODEL_NAME)->findOneOrCreateById($id);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->processSubmittedForm($form);
        }

        return $this->render(
            static::EDIT_TEMPLATE,
            $this->editActionParameters($entity, $form, $id)
        );
    }

    /**
     * @param int $id
     *
     * @return JsonResponse
     */
    public function attachedAction($id)
    {
        $attachedFiles = $this->getAttachedFiles($id);

        $files = [];
        /** @var FileInterface $file */
        foreach ($attachedFiles as $file) {
            $files[] = [
                'title' => $file->getTitle(),
                'value' => $this->generateUrl('thumbs', [
                    'directory' => $file->getDirectory(),
                    'filename' => $file->getFilename(),
                    'width' => 300,
                    'height' => 0,
                ]),
            ];
        }

        return new JsonResponse($files);
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $pagination = $this->get(static::MODEL_NAME)
            ->getItems($request)
        ;

        return $this->render(
            static::LISTING_TEMPLATE,
            $this->indexActionParameters($pagination)
        );
    }

    /**
     * @param \Kisphp\Entity\ToggleableInterface $entity
     *
     * @return $this
     */
    protected function toggleStatus(ToggleableInterface $entity)
    {
        if ($entity->getStatus() === Status::INACTIVE) {
            return $entity->setStatus(Status::ACTIVE);
        }

        return $entity->setStatus(Status::INACTIVE);
    }

    /**
     * @param \Kisphp\Entity\KisphpEntityInterface $entity
     * @param \Symfony\Component\Form\FormInterface $form
     * @param int $id
     *
     * @return array
     */
    protected function editActionParameters(KisphpEntityInterface $entity, FormInterface $form, $id)
    {
        return [
            'id' => $id,
            'entity' => $entity,
            'form' => $form->createView(),
            'attached' => $this->getAttachedFiles($id),
            'section' => $this->section,
            'html_editor' => static::HTML_EDITOR_ENABLED,
            'upload_type' => static::UPLOAD_TYPE,
            'images_list_url' => $this->getImagesEditUrl($id),
            'templates' => $this->get('model.cms.template')->getTemplatesFormEditor(),
            'section_title' => static::SECTION_TITLE . (($id > 0) ? '.edit' : '.create'),
        ];
    }

    /**
     * @param \Knp\Component\Pager\Pagination\PaginationInterface $pagination
     *
     * @return array
     */
    protected function indexActionParameters(PaginationInterface $pagination)
    {
        return [
            'section_title' => static::SECTION_TITLE . '.list',
            'pagination' => $pagination,
            'items' => $pagination->getItems(),
            'section' => $this->section,
            'allow_search' => static::ALLOW_SEARCH,
        ];
    }

    /**
     * Translates the given message.
     *
     * @param string      $id         The message id (may also be an object that can be cast to string)
     * @param array       $parameters An array of parameters for the message
     * @param null|string $domain     The domain for the message or null to use the default
     * @param null|string $locale     The locale or null to use the default
     *
     * @return string The translated string
     *
     * @throws InvalidArgumentException If the locale contains invalid characters
     */
    protected function trans($id, array $parameters = [], $domain = null, $locale = null)
    {
        return $this->get('translator')->trans($id, $parameters, $domain, $locale);
    }

    /**
     * this is used for tiny mce attached images and returns the path to json images list.
     *
     * @param int $id
     *
     * @return string
     */
    protected function getImagesEditUrl($id)
    {
        return '';
    }

    /**
     * @param Form $form
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function processSubmittedForm(Form $form)
    {
        $entity = $this->saveObjectEntity($form->getData());

        return $this->redirectAfterSave($entity);
    }

    /**
     * @param KisphpEntityInterface $entity
     * @param string $message
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    protected function redirectAfterSave(KisphpEntityInterface $entity, $message = 'Successfull saved')
    {
        $this->addFlash('success', $message);

        return $this->redirect(
            $this->generateUrl($this->section[static::EDIT_PATH], [
                'id' => $entity->getId(),
            ])
        );
    }

    /**
     * @param int $objectId
     *
     * @return array
     */
    protected function getAttachedFiles($objectId)
    {
        return [];
    }

    /**
     * @param int $id
     *
     * @return KisphpEntityInterface
     */
    protected function createFormEntity($id)
    {
        $model = $this->get(static::MODEL_NAME);

        $object = $model->find($id);
        if (empty($object)) {
            $object = $model->createEntity();
        }

        return $object;
    }

    /**
     * @param KisphpEntityInterface $data
     *
     * @return KisphpEntityInterface
     */
    protected function saveObjectEntity(KisphpEntityInterface $data)
    {
        $model = $this->get(static::MODEL_NAME);

        return $model->save($data);
    }

    /**
     * @param Request $request
     * @param int $id
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createEditForm(Request $request, $id)
    {
        $object = $this->createFormEntity($id);

        return $this->createFormObject($request, $object);
    }

    /**
     * @param Request $request
     * @param $object
     *
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createFormObject(Request $request, $object)
    {
        $form = $this->createForm(static::ENTITY_FORM_CLASS, $object);
        $form->handleRequest($request);

        return $form;
    }
}
