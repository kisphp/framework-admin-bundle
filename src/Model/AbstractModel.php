<?php

namespace Kisphp\FrameworkAdminBundle\Model;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Kisphp\Entity\KisphpEntityInterface;
use Kisphp\Model\AbstractModel as KisphpModel;
use Kisphp\Utils\Status;
use Knp\Component\Pager\Paginator;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method EntityRepository getRepository()
 */
abstract class AbstractModel extends KisphpModel
{
    const ROWS_PER_PAGE = 20;

    /**
     * @var \Knp\Component\Pager\Paginator
     */
    protected $knpPaginator;

    /**
     * @param \Doctrine\ORM\EntityManagerInterface $em
     * @param \Knp\Component\Pager\Paginator $paginator
     *
     * @throws \Kisphp\Exceptions\ConstantNotFound
     */
    public function __construct(EntityManagerInterface $em, Paginator $paginator = null)
    {
        parent::__construct($em);

        $this->knpPaginator = $paginator;
    }

    /**
     * @param null|\Symfony\Component\HttpFoundation\Request $request
     *
     * @return KisphpEntityInterface[]|\Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getItems(Request $request = null)
    {
        $query = $this->getItemsQuery($request)->getQuery();

        if ($this->knpPaginator === null) {
            return $query->getResult();
        }

        return $this->knpPaginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            static::ROWS_PER_PAGE
        );
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    protected function getItemsQuery(Request $request = null)
    {
        $query = $this->getRepository()
            ->createQueryBuilder('a')
            ->where('a.status > :status')
            ->setParameter('status', Status::DELETED)
        ;

        $this->addQueryCriteria($request, $query);

        $query->orderBy('a.id', 'DESC');

        return $query;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Doctrine\ORM\QueryBuilder $query
     */
    protected function addQueryCriteria(Request $request, QueryBuilder $query)
    {
        /** @var Request $request */
        $searchParam = $request->query->get('q', '');
        if (!$searchParam) {
            return;
        }

        if ((int) $searchParam > 0) {
            $query->andWhere('a.id = :id')
                ->setParameter('id', (int) $searchParam)
            ;

            return;
        }

        $this->searchByFields($query, $searchParam);
    }

    /**
     * @param \Doctrine\ORM\QueryBuilder $query
     * @param string $searchParam
     */
    protected function searchByFields(QueryBuilder $query, $searchParam)
    {
        $query->andWhere('a.title LIKE :qa OR a.body LIKE :qb')
            ->setParameter('qa', '%' . $searchParam . '%')
            ->setParameter('qb', '%' . $searchParam . '%');
    }
}
