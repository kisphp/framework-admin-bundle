<?php

namespace Kisphp\FrameworkAdminBundle\Twig;

use Kisphp\FrameworkAdminBundle\Twig\Filters\RepeatFilter;
use Kisphp\FrameworkAdminBundle\Twig\Functions\GenerateCodeFunction;
use Kisphp\FrameworkAdminBundle\Twig\Functions\GetLastDeploymentFunction;
use Kisphp\FrameworkAdminBundle\Twig\Functions\MatchFunction;
use Kisphp\FrameworkAdminBundle\Twig\Functions\StatusToggleFunction;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FrameworkExtensions extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            RepeatFilter::create(),
        ];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            MatchFunction::create(),
            StatusToggleFunction::create(),
            GenerateCodeFunction::create(),
            $this->createLastDeploymentFunction(),
        ];
    }

    /**
     * @return \Twig_SimpleFunction
     */
    protected function createLastDeploymentFunction()
    {
        $appDirectory = $this->container->get('kernel')->getRootDir();
        $lastDeployment = new GetLastDeploymentFunction($appDirectory);

        return $lastDeployment->getExtension();
    }
}
