<?php

namespace Kisphp\FrameworkAdminBundle\Twig\Filters;

use Kisphp\Twig\AbstractTwigFilter;
use Kisphp\Twig\IsSafeHtml;

class RepeatFilter extends AbstractTwigFilter
{
    use IsSafeHtml;

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'repeat';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function ($string, $repetitions) {
            if ($repetitions === 0) {
                return null;
            }

            return str_repeat($string, $repetitions);
        };
    }
}
