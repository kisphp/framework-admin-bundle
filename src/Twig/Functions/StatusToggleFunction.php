<?php

namespace Kisphp\FrameworkAdminBundle\Twig\Functions;

use Kisphp\Entity\ToggleableInterface;
use Kisphp\Twig\AbstractTwigFunction;
use Kisphp\Twig\IsSafeHtml;
use Kisphp\Utils\Status;

class StatusToggleFunction extends AbstractTwigFunction
{
    use IsSafeHtml;

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'statusToggle';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function (ToggleableInterface $entity, $url, $readOnly = false) {
            return $this->generateStatusToggleButton($entity, $url, $readOnly);
        };
    }

    /**
     * @param ToggleableInterface $entity
     * @param string $url
     * @param bool $readOnly
     *
     * @return string
     */
    protected function generateStatusToggleButton(ToggleableInterface $entity, $url, $readOnly = false)
    {
        $class = 'fa-toggle-off text-default';
        if ($entity->getStatus() === Status::ACTIVE) {
            $class = 'fa-toggle-on text-success';
        }

        if ($readOnly) {
            return '<td width="80"><i class="fa fa-2x ' . $class . '"></i></td>';
        }

        $html = '<td width="80"><a href="#" class="status-toggle" 
data-id="' . $entity->getId() . '" 
id="status-' . $entity->getId() . '" 
data-placement="top" 
data-toggle="tooltip" 
data-original-title="Active / Inactive" 
data-url="' . $url . '"><i class="fa fa-2x ' . $class . '"></i></a></td>';

        return $html;
    }
}
