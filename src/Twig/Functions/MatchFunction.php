<?php

namespace Kisphp\FrameworkAdminBundle\Twig\Functions;

use Kisphp\Twig\AbstractTwigFunction;

class MatchFunction extends AbstractTwigFunction
{
    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'match';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function ($pattern, $string) {
            return preg_match('/' . $pattern . '/', $string);
        };
    }
}
