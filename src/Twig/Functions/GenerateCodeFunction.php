<?php

namespace Kisphp\FrameworkAdminBundle\Twig\Functions;

use Kisphp\Twig\AbstractTwigFunction;
use Kisphp\Utils\Strings;

class GenerateCodeFunction extends AbstractTwigFunction
{
    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'generateCode';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function ($length) {
            return Strings::generateCode($length);
        };
    }
}
