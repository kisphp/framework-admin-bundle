<?php

namespace Kisphp\FrameworkAdminBundle\Twig\Functions;

use Kisphp\Twig\AbstractTwigFunction;

class GetLastDeploymentFunction extends AbstractTwigFunction
{
    /**
     * @var string
     */
    protected $rootDirPath;

    /**
     * @param string $rootDirPath
     */
    public function __construct($rootDirPath)
    {
        parent::__construct();

        $this->rootDirPath = $rootDirPath;
    }

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'get_last_deployment';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function () {
            return $this->getLastDeployment();
        };
    }

    /**
     * @return string
     */
    protected function getLastDeployment()
    {
        $appDirectory = realpath($this->rootDirPath . '/../');

        $path = $appDirectory . '/last_deploy.time';
        if (!is_file($path)) {
            return 'Development mode';
        }

        $timestamp = trim(file_get_contents($path));

        $date = new \DateTime();
        $date->setTimestamp($timestamp);

        return $date->format('Y-m-d G:i');
    }
}
