<?php

namespace Kisphp\FrameworkAdminBundle\Listener;

use Psr\Container\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class AdminEventListener
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var \Twig_Extension
     */
    protected $twig;

    /**
     * @param \Psr\Container\ContainerInterface $container
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->twig = $this->container->get('twig');
    }

    /**
     * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $this->createAdminNavigation();
    }

    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    protected function createAdminNavigation()
    {
        /** @var \AppBundle\Services\KisphpAdminMenuManager $menuManager */
        $menuManager = $this->container->get('menu_manager');
        $menuItems = $menuManager->getMenuItems();

        $this->twig->addGlobal('navigation', $menuItems);
    }
}
