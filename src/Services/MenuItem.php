<?php

namespace Kisphp\FrameworkAdminBundle\Services;

abstract class MenuItem
{
    public const ROLES = '*';

    /**
     * @param \ArrayIterator $iterator
     */
    abstract public function getMenuItems(\ArrayIterator $iterator);
}
