<?php

namespace Kisphp\FrameworkAdminBundle\Services;

use Kisphp\SecurityBundle\Entity\AdminUser;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

abstract class MenuManager
{
    /**
     * @var string[]
     */
    protected $menuItems = [];

    /**
     * @var string[]
     */
    protected $userRoles = [];

    /**
     * @var string
     */
    protected $env;

    /**
     * @param \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage $tokenStorage
     * @param mixed $env
     */
    public function __construct(TokenStorage $tokenStorage, $env)
    {
        if ($tokenStorage->getToken() !== null) {
            $user = $tokenStorage->getToken()->getUser();
            if ($user instanceof AdminUser) {
                $this->userRoles = $user->getRoles();
            }
        }

        $this->menuItems = $this->registerMenuItems();
        $this->env = $env;
    }

    /**
     * Register menu items classes here.
     *
     * return [
     *      ArticlesMenuItems::class,
     *      CmsMenuItems::class,
     * ];
     *
     * @return array
     */
    abstract public function registerMenuItems();

    /**
     * @return \ArrayIterator
     */
    public function getMenuItems()
    {
        $iterator = $this->createIterator();
        foreach ($this->menuItems as $item) {
            $this->createMenuItemInstance($iterator, $item);
        }

        return $iterator;
    }

    /**
     * @return \ArrayIterator
     */
    protected function createIterator()
    {
        return new \ArrayIterator();
    }

    /**
     * @param \ArrayIterator $iterator
     * @param string $menuClassNamespace
     */
    protected function createMenuItemInstance(\ArrayIterator $iterator, $menuClassNamespace)
    {
        if ($this->checkPermissions($menuClassNamespace)) {
            /** @var MenuItemInterface $nav */
            $nav = new $menuClassNamespace();
            $nav->getMenuItems($iterator);
        }
    }

    /**
     * @param string $className
     *
     * @return bool
     */
    protected function checkPermissions($className)
    {
        if (defined("${className}::ROLES") === false) {
            // if ROLES constant is not defined, allow access for backwards compatibility
            return true;
        }
        // super admin can see all items
        if (in_array('ROLE_SUPER_ADMIN', $this->userRoles, true)) {
            return true;
        }

        // display all menu items in tests execution
        if ($this->env === 'test') {
            return true;
        }

        $permissions = constant("${className}::ROLES");
        if ($permissions === '*') {
            return true;
        }

        // allow admin to see all items, except for the super admin required
        if ($permissions !== 'ROLE_SUPER_ADMIN' && in_array('ROLE_ADMIN', $this->userRoles, true)) {
            return true;
        }

        if (in_array($permissions, $this->userRoles, true)) {
            return true;
        }

        return false;
    }
}
