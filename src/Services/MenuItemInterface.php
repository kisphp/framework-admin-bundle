<?php

namespace Kisphp\FrameworkAdminBundle\Services;

/**
 * @deprecated use MenuItem abstract class instead
 */
interface MenuItemInterface
{
    /**
     * @param \ArrayIterator $iterator
     */
    public function getMenuItems(\ArrayIterator $iterator);
}
