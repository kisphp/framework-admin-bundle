<?php

namespace Kisphp\FrameworkAdminBundle\Services\Menu;

use Kisphp\FrameworkAdminBundle\Services\MenuItemInterface;

/**
 * This class is only as an example.
 */
class DemoMenuItems implements MenuItemInterface
{
    /**
     * @param \ArrayIterator $iterator
     */
    public function getMenuItems(\ArrayIterator $iterator)
    {
        $iterator->append([
            'is_header' => false,
            'valid_feature' => 'demo',
            'match_route' => 'demo',
            'path' => 'adm_demo',
            'icon' => 'fa-bullet',
            'label' => 'main_navigation.demo',
        ]);
    }
}
