<?php

namespace Kisphp\FrameworkAdminBundle\Command\Collector;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WidgetsCommand extends AbstractCms
{
    protected function configure()
    {
        $this->setName('collector:widgets')
            ->setDescription('Collect all widgets content')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('cms.manager')->exportWidgets($output);
    }
}
