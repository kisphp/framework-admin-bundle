<?php

namespace Kisphp\FrameworkAdminBundle\Command\Collector;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ServicesCommand extends AbstractCms
{
    protected function configure()
    {
        $this->setName('collector:services')
            ->setDescription('Export services')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $manager = $this->getContainer()->get('cms.manager');

        $manager->exportServicesForTopMenu($output);
        $manager->exportEachService($output);
    }
}
