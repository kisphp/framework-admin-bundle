<?php

namespace Kisphp\FrameworkAdminBundle\Command\Collector;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AllCommand extends AbstractCms
{
    protected function configure()
    {
        $this->setName('collector:all')
            ->setDescription('Run all collectors')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('collector.manager')->runAllExporters($output);
    }
}
