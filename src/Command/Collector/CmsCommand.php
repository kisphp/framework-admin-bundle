<?php

namespace Kisphp\FrameworkAdminBundle\Command\Collector;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CmsCommand extends AbstractCms
{
    protected function configure()
    {
        $this->setName('collector:cms')
            ->setDescription('Collect CMS data')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('cms.manager')->exportLayouts($output);
    }
}
