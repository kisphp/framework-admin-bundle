<?php

namespace Kisphp\FrameworkAdminBundle\Command\Collector;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanCommand extends AbstractCms
{
    protected function configure()
    {
        $this->setName('collector:clean')
            ->setDescription('Clean colleted data')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('cms.manager')->cleanExported($output);
    }
}
