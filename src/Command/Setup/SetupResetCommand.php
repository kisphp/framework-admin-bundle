<?php

namespace Kisphp\FrameworkAdminBundle\Command\Setup;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetupResetCommand extends ContainerAwareCommand
{
    const DOCTRINE_SCHEMA_DROP = 'doctrine:schema:drop';

    protected function configure()
    {
        $this->setName('setup:reset')
            ->setDescription('Delete database tables and recreate them')
        ;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     *
     * @return null|int|void
     *
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = $this->getApplication()->find(self::DOCTRINE_SCHEMA_DROP);

        $arguments = [
            'command' => self::DOCTRINE_SCHEMA_DROP,
//            '--dump-sql' => true,
            '--force' => true,
        ];

        $cmdInput = new ArrayInput($arguments);
        $command->run($cmdInput, $output);
    }
}
