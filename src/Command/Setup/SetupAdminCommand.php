<?php

namespace Kisphp\FrameworkAdminBundle\Command\Setup;

use AppBundle\Entity\AdminUser;
use Kisphp\Utils\Status;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetupAdminCommand extends ContainerAwareCommand
{
    public const ADMIN_EMAIL = 'admin@example.com';
    public const ADMIN_PASS = 'admin';

    protected function configure()
    {
        $this
            ->setName('setup:admin')
            ->setDescription('Add Admin account for development use')
            ->addArgument('email', InputArgument::OPTIONAL, 'Email address for the new user')
            ->addArgument('password', InputArgument::OPTIONAL, 'Password for the new user')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $modelAdmin = $this->getContainer()->get('model.admin_user');

        $email = self::ADMIN_EMAIL;
        $pass = self::ADMIN_PASS;

        $emailArgument = $input->getArgument('email');
        $passArgument = $input->getArgument('password');

        if (!empty($emailArgument)) {
            if (empty($passArgument)) {
                $output->writeln('<error>Email and password are mandatory in this case</error>');

                return;
            }

            $email = $emailArgument;
            $pass = $passArgument;
        }

        $admin = $modelAdmin->findOneBy([
            'email' => $email,
        ]);
        if (empty($admin)) {
            $admin = new AdminUser();
        }

        $password = $this->getContainer()
            ->get('security.password_encoder')
            ->encodePassword($admin, $pass)
        ;

        $admin->setId(1);
        $admin->setEmail($email);
        $admin->setPassword($password);
        $admin->setStatus(Status::ACTIVE);
        $admin->setRoles(['ROLE_SUPER_ADMIN']);
        $admin->setName('Admin');
        $admin->setUsername('Web Master');
        $admin->setRegistered(new \DateTime('now'));

        $modelAdmin->save($admin);

        $output->writeln('Created admin user with email: <info>' . $email . '</info> and password: <info>' . $pass . '</info>');
    }
}
