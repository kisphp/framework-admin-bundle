<?php

namespace Kisphp\FrameworkAdminBundle\Command;

use Kisphp\FrameworkAdminBundle\Fixtures\DemoDataInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DemoDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('demo:data')
            ->setDescription('Generates demo data for website')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $availableBundles = $this->getContainer()->get('kernel')->getBundles();
        foreach ($availableBundles as $bundle) {
            $fixturesDirectory = $bundle->getPath() . '/DemoData/';
            if (!is_dir($fixturesDirectory)) {
                continue;
            }

            foreach (new \DirectoryIterator($fixturesDirectory) as $demoFile) {
                if (!$demoFile->isFile()) {
                    continue;
                }

                $className = $bundle->getNamespace() . '\\DemoData\\' . str_replace('.php', '', $demoFile->getBasename());

                /** @var \Kisphp\FrameworkAdminBundle\Fixtures\DemoDataInterface $object */
                $object = new $className($this->getContainer());
                if ($object instanceof DemoDataInterface) {
                    $object->loadDemoData($input, $output);
                }
            }
        }
    }
}
