<?php

namespace KisphpFrameworkAdminBundle\Helper\Unit;

use Kisphp\FrameworkAdminBundle\Services\MenuItem;

class DummyMenuItem extends MenuItem
{
    /**
     * @param \ArrayIterator $iterator
     */
    public function getMenuItems(\ArrayIterator $iterator)
    {
        $iterator->append([
            'is_header' => true,
            'valid_feature' => 'admin',
            'label' => 'Admin',
        ]);
    }
}
