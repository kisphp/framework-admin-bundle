<?php

namespace KisphpFrameworkAdminBundle\Helper\Unit;

use Kisphp\FrameworkAdminBundle\Services\MenuManager;

class DummyMenuManager extends MenuManager
{
    /**
     * @return array
     */
    public function registerMenuItems()
    {
        return [
            DummyMenuItem::class,
        ];
    }
}
