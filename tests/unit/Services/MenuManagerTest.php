<?php

namespace Kisphp\FrameworkAdminBundle\Unit\Services;

use Codeception\Test\Unit;
use Kisphp\SecurityBundle\Entity\AdminUser;
use KisphpFrameworkAdminBundle\Helper\Unit\DummyMenuManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class MenuManagerTest extends Unit
{
    /**
     * @var DummyMenuManager
     */
    protected $mm;

    protected function setUp() : void
    {
        $user = \Mockery::mock(AdminUser::class);
        $user->shouldReceive('getRoles')->andReturn(['ROLE_SUPER_ADMIN']);

        $token = \Mockery::mock(TokenStorage::class);
        $token->shouldReceive('getToken')->andReturnSelf();
        $token->shouldReceive('getUser')->andReturn($user);

        $this->mm = new DummyMenuManager($token, 'test');

        parent::setUp();
    }

    public function test_menu_manager_has_items()
    {
        $this->assertGreaterThan(0, $this->mm->getMenuItems()->count());
    }

    public function test_menu_manager_returns_array_iterator()
    {
        $this->assertInstanceOf(\ArrayIterator::class, $this->mm->getMenuItems());
    }
}
