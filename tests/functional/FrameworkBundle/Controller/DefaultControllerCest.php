<?php

namespace Tests\Functional\AdminBundle\Controller;

use Kisphp\CmsBundle\DemoData\CmsDemo;
use Kisphp\FrameworkAdminBundle\Command\DemoDataCommand;

/**
 * @group framework
 */
class DefaultControllerCest
{
    /**
     * @param \FunctionalTester $i
     */
    public function index_page(\FunctionalTester $i)
    {
        $i->amOnPage('/');
        $i->see('Logout');
        $i->canSeeResponseCodeIs(200);
    }
}
